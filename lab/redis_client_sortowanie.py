
import time, timeit
import redis_worker_sortowanie


compute_range = range(10)


def compute_square():
    return redis_worker_sortowanie.sort.delay()


def make_request():
    return compute_square()


def requests_suite():
    requests = [make_request() for i in compute_range]
    done = False
    while not done:
        done = True
        for i in compute_range:
            result = requests[i].result
            if result is None:
                done = False

    for i in compute_range:
        result = requests[i].result
        print result
    time.sleep(0.2)


print 'Five successive runs:\n\n',
for i in range(1, 4):
    print '%.2fs\n\n' % (timeit.timeit(requests_suite, number=1)),