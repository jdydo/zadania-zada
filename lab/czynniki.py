from math import *
import memcache
import random
import timeit
import time


mc = memcache.Client(['194.29.175.241:11211'])


def pobierz_pierwiastek(x):
    p = mc.get('p1_p_%s' % str(x))
    if not p:
        p = floor(sqrt(x))
        mc.set('p1_p_%s' % str(x), p)
    return p


def pobierz_modulo(x, i):
    m = mc.get('p1_m_%s_%s' % (str(x), str(i)))
    if not m:
        m = x % i
        mc.set('p1_m_%s_%s' % (str(x), str(i)), m)
    return m


def pobierz_rozklad(x):
    n = x
    r = mc.get('p1_%s' % str(n))
    if not r:
        i = 2
        p = pobierz_pierwiastek(x)
        r = []
        while i <= p:
            if pobierz_modulo(x, i) == 0:
                r.append(i)
                x /= i
                p = pobierz_pierwiastek(x)
            else:
                i += 1
        if x > 1:
            r.append(x)
        mc.set('p1_%s' % str(n), r)
    return r


def rozklad(x):
    if x <= 1:
        raise ValueError('Niewlasciwe dane!')
    else:
        return pobierz_rozklad(x)


def test():
    x = random.randint(2, 100)
    print x, ' ---> ',
    print rozklad(x)

print 'Timer: %.2fs' % timeit.timeit(test, number=200)

