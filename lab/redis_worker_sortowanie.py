import redis
from rq.decorators import job
import requests


conn = redis.StrictRedis(host='194.29.175.241')


@job('p1_sort', connection=conn)
def sort():
    r = requests.get("http://www.random.org/integers/?num=10&min=0&max=100&col=1&base=10&format=plain&rnd=new")
    splited = r.content.split('\n')
    int_list = list()
    for number in splited:
        try:
            int_list.append(int(number))
        except ValueError:
            pass
    int_list.sort()
    return int_list