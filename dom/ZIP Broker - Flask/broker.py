# -*- coding: utf-8 -*-

from flask import Flask


app = Flask(__name__, static_url_path='')
app.config['DEBUG'] = True


@app.route('/')
def hello():
  return "Hello! :)"

if __name__ == '__main__':
  app.run( 
        host="194.29.175.241",
        port=int("22334")
  )

