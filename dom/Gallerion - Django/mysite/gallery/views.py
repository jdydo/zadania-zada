from django.views.generic import View
from django.shortcuts import render
from forms import *
from django.contrib.auth.models import User
from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.core.urlresolvers import reverse
from django.http import Http404
from gallery.tasks import *
from PIL import Image
from django.core.files.uploadedfile import SimpleUploadedFile
import os
from cStringIO import StringIO
from mysite.settings import *
import zipfile
from celery.result import AsyncResult
from django.http import HttpResponse, HttpResponseRedirect


class CreateFolder(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'create_folder.html', {'form': CreateFolderForm(initial={'user': request.user})})

    def post(self, request, *args, **kwargs):
        form = CreateFolderForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            user = form.cleaned_data['user']

            if Folder.objects.all().filter(user=user).filter(name=name):
                return render(request, 'create_folder.html',
                              {'form': form, 'error': 'Folder o tej nazwie juz istnieje!'})
            else:
                form.save()
                return render(request, 'create_folder.html',
                              {'status': 'OK', 'form': CreateFolderForm(initial={'user': request.user})})
        else:
            return render(request, 'create_folder.html', {'form': form})


class ImageResizingWorkStatus(View):
    def get(self, request, *args, **kwargs):
        if 'job' in request.GET:
            job_id = request.GET['job']
        else:
            return HttpResponse('No job id given.')

        job = AsyncResult(job_id)
        time.sleep(0.5)
        if job.state == 'PENDING':
            return render(request, 'progress_bar_send_image.html', {'result': -1, 'status': 'Obraz przeslany. Oczekiwanie na Workera...'})

        elif job.state == 'PROGRESS':
            try:
                result = job.result['current']
            except TypeError:
                result = 99
                print 'aaa'
            return render(request, 'progress_bar_send_image.html', {'result': result, 'status': 'Obraz przeslany. Tworzenie miniatury...'})

        elif job.state == 'SUCCESS':
            thumbnail = job.result[0]
            content = job.result[1]
            pk = job.result[2]
            time.sleep(0.5)
            base_image = ImageModel.objects.all().filter(pk=pk).first()
            self.thumber_helper(base_image, thumbnail['large'], 'large', content)
            self.thumber_helper(base_image, thumbnail['medium'], 'medium', content)
            self.thumber_helper(base_image, thumbnail['small'], 'small', content)
            base_image = ImageModel.objects.all().filter(pk=pk).first()
            return render(request, 'progress_bar_send_image.html', {'result': 100, 'image': base_image})

        else:
            return render(request, 'progress_bar_send_image.html', {'result': -2})

    def thumber_helper(self, base_image, thumb, thumb_type, content):
        file_extension = content.split('/')[1]

        returned_stringed_image = StringIO(thumb)
        returned_opened_image = Image.open(returned_stringed_image)

        img_to_str_converter = StringIO()
        returned_opened_image.save(img_to_str_converter, file_extension)
        img_to_str_converter.seek(0)

        suf = SimpleUploadedFile(os.path.split(base_image.picture.name)[-1], img_to_str_converter.read(), content_type=content)

        if thumb_type == 'large':
            base_image.picture_thumb_large.save(
                '%s_thumbnail_%s.%s' % (os.path.splitext(suf.name)[0], 'large', file_extension), suf, save=False)
        elif thumb_type == 'medium':
            base_image.picture_thumb_medium.save(
                '%s_thumbnail_%s.%s' % (os.path.splitext(suf.name)[0], 'medium', file_extension), suf, save=False)
        elif thumb_type == 'small':
            base_image.picture_thumb_small.save(
                '%s_thumbnail_%s.%s' % (os.path.splitext(suf.name)[0], 'small', file_extension), suf, save=False)

        base_image.save()


class DownloadZip(View):
    def get(self, request, *args, **kwargs):
        if 'job' in request.GET:
            job_id = request.GET['job']
        else:
            return HttpResponse('No job id given.')

        job = AsyncResult(job_id)
        time.sleep(0.5)
        if job.state == 'SUCCESS':
            # zip_size = job.result[0]
            # zip_file = job.result[1]
            # time.sleep(0.5)
            # print zip_size
            # print len(zip_file)
            # while len(zip_file) != zip_size:
            #     pass
            # print 'bbbb'
            # resp = HttpResponse(zip_file, mimetype = "application/x-zip-compressed")
            # resp['Content-Disposition'] = 'attachment; filename=obrazki.zip'
            #
            # return resp
            res = job.result
            time.sleep(0.5)
            from django.shortcuts import redirect
            return redirect(res)

        # elif job.state == 'PROGRESS':
        #     zip_size = job.result[0]
        #     zip_file = job.result[1]
        #     time.sleep(0.5)
        #     print zip_size
        #     print len(zip_file)
        #     while len(zip_file) != zip_size:
        #         pass
        #     print 'ccc'
        #     resp = HttpResponse(zip_file, mimetype = "application/x-zip-compressed")
        #     resp['Content-Disposition'] = 'attachment; filename=obrazki.zip'
        #
        #     return resp

        else:
            print job.state
            return render(request, 'progress_bar_create_zip.html', {'result': -2})


class ZippinWorkStatus(View):
    def get(self, request, *args, **kwargs):
        if 'job' in request.GET:
            job_id = request.GET['job']
        else:
            return HttpResponse('No job id given.')

        job = AsyncResult(job_id)
        if job.state == 'PENDING':
            return render(request, 'progress_bar_create_zip.html', {'result': -1})

        elif job.state == 'PROGRESS':
            try:
                result = job.result['current']
            except TypeError:
                result = 99
            return render(request, 'progress_bar_create_zip.html', {'result': result, 'job_id': job.id})

        elif job.state == 'SUCCESS':
            return render(request, 'progress_bar_create_zip.html', {'result': 100, 'job_id': job.id})
        else:
            print job.state
            return render(request, 'progress_bar_create_zip.html', {'result': -2})


class DownloadResizedImage(View):
    def get(self, request, *args, **kwargs):
        if 'job' in request.GET:
            job_id = request.GET['job']
        else:
            return HttpResponse('No job id given.')

        job = AsyncResult(job_id)
        time.sleep(0.5)
        if job.state == 'SUCCESS':
            image_size = job.result[0]
            image_file = job.result[1]
            time.sleep(0.5)
            while len(image_file) != image_size:
                pass
            resp = HttpResponse(image_file, mimetype="image/jpg")
            resp['Content-Disposition'] = 'attachment; filename=obraz.jpg'
            return resp
        else:
            print job.state
            return render(request, 'progress_bar_create_zip.html', {'result': -2})


class ResisingImageStatus(View):
    def get(self, request, *args, **kwargs):
        if 'job' in request.GET:
            job_id = request.GET['job']
        else:
            return HttpResponse('No job id given.')

        job = AsyncResult(job_id)
        if job.state == 'PENDING':
            return render(request, 'progress_bar_resize_image.html', {'result': -1})

        elif job.state == 'PROGRESS':
            try:
                result = job.result['current']
            except TypeError:
                result = 99
            return render(request, 'progress_bar_resize_image.html', {'result': result, 'job_id': job.id})

        elif job.state == 'SUCCESS':
            return render(request, 'progress_bar_resize_image.html', {'result': 100, 'job_id': job.id})
        else:
            return render(request, 'progress_bar_resize_image.html', {'result': -2})


class SendImage(View):
    def get(self, request, *args, **kwargs):
        form = SendImageForm(initial={'user': request.user})
        form.fields["folder"].queryset = Folder.objects.filter(user=request.user)
        form.fields['name'].widget.attrs['class'] = "form-control"
        form.fields['folder'].widget.attrs['class'] = "form-control"
        return render(request, 'send_image.html', {'form': form})

    def post(self, request, *args, **kwargs):
        form = SendImageForm(request.POST, request.FILES)
        form.fields["folder"].queryset = Folder.objects.filter(user=request.user)
        form.fields['name'].widget.attrs['class'] = "form-control"
        form.fields['folder'].widget.attrs['class'] = "form-control"
        if form.is_valid():
            folder = Folder.objects.all().filter(name=form.cleaned_data['folder'].name)
            image = ImageModel.objects.all().filter(user=request.user).filter(folder=folder).filter(name=form.cleaned_data['name']).first()
            if image:
                return render(request, 'send_image.html', {'form': form, 'error': 'Obrazek o tej nazwie juz istnieje'})
            else:
                image_instance = form.instance
                content_type = image_instance.picture.file.content_type
                image_instance.save()

                job = create_thumb.delay('%s%s' % (SERVER_URL, image_instance.picture.url), content_type, image_instance.pk)

                return HttpResponseRedirect(reverse('image_send_state') + '?job=' + job.id)

        else:
            return render(request, 'send_image.html', {'form': form})


class Config(View):
    def get(self, request, *args, **kwargs):
        form = ConfigForm(initial={'user': request.user})
        return render(request, 'config.html', {'form': form})

    def post(self, request, *args, **kwargs):
        form = ConfigForm(request.POST)
        if form.is_valid():
            old_config = ConfigModel.objects.all().filter(user=request.user).first()
            if old_config:
                old_config.thumb_type = form.cleaned_data['thumb_type']
                old_config.save()
            else:
                form.save()
            return render(request, 'config.html', {'form': ConfigForm(initial={'user': request.user}), 'status': 'ok'})
        else:
            return render(request, 'config.html', {'form': form})


class GetZippedFolder(View):
    def get(self, request, user_name, folder_name):

        user = User.objects.all().filter(username=user_name)
        folder = Folder.objects.all().filter(name=folder_name)
        images = ImageModel.objects.all().filter(user=user).filter(folder=folder)

        url_table = []
        for image in images:
            url_table.append('%s%s' % (SERVER_URL, image.picture.url))

        job = create_zip.delay(url_table)
        return HttpResponseRedirect(reverse('get_zip_state') + '?job=' + job.id)


class ResizeImage(View):
    def get(self, request, username, foldername, imagename, width):
        user = User.objects.all().filter(username=username)
        folder = Folder.objects.all().filter(name=foldername)
        image = ImageModel.objects.all().filter(user=user).filter(folder=folder).filter(name=imagename).first()
        image_url = '%s%s' % (SERVER_URL, image.picture.url)

        job = resize.delay(image_url, width)
        return HttpResponseRedirect(reverse('get_resize_state') + '?job=' + job.id)


class GetUserFolders(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'user_folder_list.html', {'folder_list': User.objects.all()})


class GetFolders(View):
    def get(self, request, username):
        user = User.objects.all().filter(username=username).first()
        return render(request, 'folder_list.html', {'folder_list': Folder.objects.all().filter(user=user)})


class GetImages(View):
    def get(self, request, username, foldername):
        user = User.objects.all().filter(username=username).first()
        folder = Folder.objects.all().filter(name=foldername).first()
        if request.user.is_authenticated():
            thumb_type = ConfigModel.objects.all().filter(user=request.user).first()
            if thumb_type:
                thumb_type = thumb_type.thumb_type
            else:
                thumb_type = 'medium'
        else:
            thumb_type = 'medium'

        return render(request,
                      'image_list.html',
                      {'image_list': ImageModel.objects.all().filter(user=user).filter(folder=folder),
                       'type': thumb_type, 'username': username, 'foldername': foldername })


class GetImage(View):
    def get(self, request, username, foldername, imagename):
        form = ChangeFolderForm()
        user = User.objects.all().filter(username=username)
        folder = Folder.objects.all().filter(name=foldername)
        image = ImageModel.objects.all().filter(user=user).filter(folder=folder).filter(name=imagename).first()
        if image:
            image_format = str(image.picture.url).rsplit('.', 1)[1]
            if image.user == request.user:
                form.fields["folder"].queryset = Folder.objects.filter(user=request.user)
                user_folder = True
            else:
                user_folder = False
            return render(request, 'show_image.html',
                          {'image': image, 'image_format': image_format, 'form': form, 'user_folder': user_folder})
        else:
            raise Http404

    def post(self, request, username, foldername, imagename):
        form = ChangeFolderForm(request.POST)
        user = User.objects.all().filter(username=username)
        folder = Folder.objects.all().filter(name=foldername)
        image = ImageModel.objects.all().filter(user=user).filter(folder=folder).filter(name=imagename).first()
        if image:
            image_format = str(image.picture.url).rsplit('.', 1)[1]
            if image.user == request.user:
                form.fields["folder"].queryset = Folder.objects.filter(user=request.user)
                user_folder = True
            else:
                user_folder = False

            if form.is_valid():
                image.folder = form.cleaned_data['folder']
                image.save()
                return render(request, 'folder_list.html',
                              {'folder_list': Folder.objects.all().filter(user=user), 'status': True})
            else:
                return render(request, 'show_image.html',
                              {'image': image, 'image_format': image_format, 'form': form, 'user_folder': user_folder})
        else:
            raise Http404


class Main(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'fullwidth.html', {'text': 'Zostales pomyslnie wylogowany!'})


class Login(View):
    def get(self, request):
        form = AuthenticationForm()
        form.fields['username'].widget.attrs['class'] = "form-control"
        form.fields['password'].widget.attrs['class'] = "form-control"
        form.fields['username'].widget.attrs['placeholder'] = "Nazwa uzytkownika"
        form.fields['password'].widget.attrs['placeholder'] = "Haslo"
        return render(request, 'login.html', {'form': form})

    def post(self, request):
        form = AuthenticationForm(data=request.POST)
        new_form = AuthenticationForm()
        new_form.fields['username'].widget.attrs['class'] = "form-control"
        new_form.fields['password'].widget.attrs['class'] = "form-control"
        new_form.fields['username'].widget.attrs['placeholder'] = "Nazwa uzytkownika"
        new_form.fields['password'].widget.attrs['placeholder'] = "Haslo"
        if form.is_valid():
            login(request, form.get_user())
            return render(request, 'ok.html', {'text': 'Zostales pomyslnie zalogowany!'})
        elif form.get_user() and not form.get_user().is_active:
            return render(request, 'login.html', {'error': 'Uzytkownik nieaktywny.', 'form': new_form})
        else:
            return render(request, 'login.html', {'error': 'Zly login lub haslo.',
                                                  'form': new_form})


class Logout(View):
    def get(self, request, *args, **kwargs):
        logout(request)
        return render(request, 'ok.html', {'text': 'Zostales pomyslnie wylogowany!'})
