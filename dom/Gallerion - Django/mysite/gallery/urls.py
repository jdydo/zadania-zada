from django.contrib.auth.decorators import login_required
from views import *
from django.conf.urls import patterns, include, url
from django.contrib import admin
from mysite.settings import SITE_ROOT
import os

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^create/folder/$', login_required(CreateFolder.as_view()), name='create_folder'),
    url(r'^send/image$', login_required(SendImage.as_view()), name='send_image'),
    url(r'^send/image/progress$', login_required(ImageResizingWorkStatus.as_view()), name='image_send_state'),

    url(r'^$', Main.as_view(), name='main'),


    url(r'^config$', login_required(Config.as_view()), name='config'),

    url(r'^filter$', GetUserFolders.as_view(), name='get_users'),
    url(r'^filter/(?P<username>.*)/(?P<foldername>.*)/(?P<imagename>.*)$', GetImage.as_view(), name='get_image'),
    url(r'^filter/(?P<username>.*)/(?P<foldername>.*)$', GetImages.as_view(), name='get_images'),
    url(r'^filter/(?P<username>.*)$', GetFolders.as_view(), name='get_folders'),

    url(r'^resize/(?P<username>.*)/(?P<foldername>.*)/(?P<imagename>.*)/(?P<width>\d+)/$', ResizeImage.as_view(), name='resize_image'),
    url(r'^resize/progress$', ResisingImageStatus.as_view(), name='get_resize_state'),
    url(r'^resize/download', DownloadResizedImage.as_view(), name='download_resized_image'),

    url(r'^zip/(?P<user_name>.*)/(?P<folder_name>.*)$', GetZippedFolder.as_view(), name='get_zipped_folder'),
    url(r'^zip/progress$', login_required(ZippinWorkStatus.as_view()), name='get_zip_state'),
    url(r'^zip/download', login_required(DownloadZip.as_view()), name='download_zip'),

    url(r'^login/$', Login.as_view(), name='login'),
    url(r'^logout/$', login_required(Logout.as_view()), name='logout'),

    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': os.path.join(SITE_ROOT, '../media/'), 'show_indexes': True}),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': os.path.join(SITE_ROOT, '../static/'), 'show_indexes': True}),

    url(r'^admin/', include(admin.site.urls)),
)
