from django.forms import ModelForm, HiddenInput, TextInput, Form, CharField
from django import forms
from models import *



class CreateFolderForm(ModelForm):

    class Meta:
        model = Folder
        fields = [
            'name', 'user'
        ]
        widgets = {
            'user': HiddenInput(),
            'name': TextInput(attrs={'placeholder': 'Wpisz nazwe folderu',
                                     'name': 'Name',
                                     'id': 'id_name',
                                     'class': 'form-control'}),
        }

    def clean_name(self):
        data = self.cleaned_data['name']
        if len(data) < 1 or len(data) > 15:
            raise forms.ValidationError("Nazwa folderu nie moze byc mniejsza niz 1 i wieksza niz 15.")
        return data


class SendImageForm(ModelForm):

    class Meta:
        model = ImageModel
        fields = [
            'name', 'picture', 'user', 'folder'
        ]
        widgets = {
            'user': HiddenInput(),
        }

    def clean_name(self):
        data = self.cleaned_data['name']
        if len(data) < 1 or len(data) > 12:
            raise forms.ValidationError("Nazwa obrazku nie moze byc mniejsza niz 1 i wieksza niz 12.")
        return data

    def clean_picture(self):
        data = self.cleaned_data.get('picture', False)
        if data:
            if data._size > 1*1024*1024:
                raise forms.ValidationError("Plik za duzy - maksimum 1 Mb.")
            return data
        else:
            raise forms.ValidationError("Nie mozna bylo odczytac pliku.")


class ChangeFolderForm(Form):
    folder = forms.ModelChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), queryset=Folder.objects.all())


class ConfigForm(ModelForm):

    class Meta:
        model = ConfigModel
        fields = [
            'thumb_type', 'user'
        ]
        THUMBNAIL_TYPE = (
            ('large', 'Duze'),
            ('medium', 'Srednie'),
            ('small', 'Male'),
        )
        widgets = {
            'user': HiddenInput(),
            'thumb_type': forms.Select(attrs={'class': 'form-control'})
        }

