from django.contrib import admin
from djcelery.models import TaskMeta
from gallery.models import *


class TaskMetaAdmin(admin.ModelAdmin):
    readonly_fields = ('result',)

admin.site.register(TaskMeta, TaskMetaAdmin)
admin.site.register(ImageModel)
admin.site.register(Folder)
admin.site.register(ConfigModel)