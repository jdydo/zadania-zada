from django.contrib.auth.models import User
from django.db import models


class Folder(models.Model):
    name = models.CharField(max_length=30)
    user = models.ForeignKey(User)

    def __unicode__(self):
        return self.name


class ImageModel(models.Model):
    picture = models.ImageField(upload_to='images/')
    picture_thumb_large = models.ImageField(upload_to='thumbs/large/', blank=True)
    picture_thumb_medium = models.ImageField(upload_to='thumbs/medium/', blank=True)
    picture_thumb_small = models.ImageField(upload_to='thumbs/small/', blank=True)
    folder = models.ForeignKey(Folder)
    user = models.ForeignKey(User)
    name = models.CharField(max_length=50)


class ChangeFolderModel(models.Model):
    folder = models.ForeignKey(Folder)

    def __unicode__(self):
        return self.folder.name


class ConfigModel(models.Model):
    THUMBNAIL_TYPE = (
        ('large', 'Duze'),
        ('medium', 'Srednie'),
        ('small', 'Male'),
    )
    thumb_type = models.CharField(max_length=20, choices=THUMBNAIL_TYPE)
    user = models.ForeignKey(User)