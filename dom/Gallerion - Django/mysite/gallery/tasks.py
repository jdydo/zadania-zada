from celery import task, current_task
from cStringIO import StringIO
import PIL
from PIL import Image, ImageOps
import urllib2
import time
import zipfile
import os
import zlib
import uuid

@task()
def create_thumb(url, content_type, pk):
    print url
    current_task.update_state(state='PROGRESS', meta={'current': 0, 'total': 100})
    time.sleep(0.5)

    file_extension = content_type.split('/')[1]
    stringed_image = urllib2.urlopen(url).read()
    current_task.update_state(state='PROGRESS', meta={'current': 15, 'total': 100})
    time.sleep(0.7)

    image = Image.open(StringIO(stringed_image))
    thumbnails = {}
    current_task.update_state(state='PROGRESS', meta={'current': 30, 'total': 100})

    image_large = ImageOps.fit(image, (270, 270), PIL.Image.ANTIALIAS)
    current_task.update_state(state='PROGRESS', meta={'current': 50, 'total': 100})
    time.sleep(0.7)

    image_medium = ImageOps.fit(image_large, (170, 170), PIL.Image.ANTIALIAS)
    current_task.update_state(state='PROGRESS', meta={'current': 60, 'total': 100})
    time.sleep(0.7)

    image_small = ImageOps.fit(image_medium, (70, 70), PIL.Image.ANTIALIAS)
    current_task.update_state(state='PROGRESS', meta={'current': 70, 'total': 100})
    time.sleep(0.7)

    output_large = StringIO()
    image_large.save(output_large, format=file_extension, quality=25)
    contents_large = output_large.getvalue()
    output_large.close()
    thumbnails['large'] = contents_large
    current_task.update_state(state='PROGRESS', meta={'current': 80, 'total': 100})
    time.sleep(1)

    output_medium = StringIO()
    image_medium.save(output_medium, format=file_extension, quality=25)
    contents_medium = output_medium.getvalue()
    output_medium.close()
    thumbnails['medium'] = contents_medium
    current_task.update_state(state='PROGRESS', meta={'current': 90, 'total': 100})
    time.sleep(1)

    output_small = StringIO()
    image_small.save(output_small, format=file_extension, quality=25)
    contents_small = output_small.getvalue()
    output_small.close()
    thumbnails['small'] = contents_small

    return thumbnails, content_type, pk


@task()
def create_zip(url_table):
    current_task.update_state(state='PROGRESS', meta={'current': 0, 'total': 100})
    time.sleep(1)

    number = len(url_table)
    i = 0
    part = int(40/number)
    image_table = []
    for url in url_table:
        image_table.append(urllib2.urlopen(url).read())
        i += part
        current_task.update_state(state='PROGRESS', meta={'current': i, 'total': 100})
        time.sleep(0.3)

    try:
        compression = zipfile.ZIP_DEFLATED
    except:
        compression = zipfile.ZIP_STORED

    modes = {zipfile.ZIP_DEFLATED: 'deflated',
             zipfile.ZIP_STORED:   'stored',
             }

    zip_subdir = "obrazki"
    #zip_filename = "%s.zip" % zip_subdir

    s = StringIO()

    #zf = zipfile.ZipFile(s, "w")
    name = str(uuid.uuid4())
    zf = zipfile.ZipFile('/home/jakub/p1/static/Obrazy_%s.zip' % name, "w")  ## TODO: Sciezka do serwera

    i1 = 40
    part = int(55/number)
    i = 0
    for image in image_table:
        i += 1
        zip_path = os.path.join(zip_subdir, 'image%s.jpg' % i)

        zf.writestr(zip_path, image, compress_type=compression)
        i1 += part
        current_task.update_state(state='PROGRESS', meta={'current': i1, 'total': 100})
        time.sleep(0.3)
    zf.close()
    #zip_file = s.getvalue()
    #size = len(zip_file)
    current_task.update_state(state='PROGRESS', meta={'current': 95, 'total': 100})

    #return size, zip_file
    return 'http://localhost:22334/Obrazy_%s.zip' % name  ## TODO: Sciezka do serwera przy flasku

@task()
def resize(url, width):
    current_task.update_state(state='PROGRESS', meta={'current': 0, 'total': 100})
    time.sleep(1)

    stringed_image = urllib2.urlopen(url).read()
    current_task.update_state(state='PROGRESS', meta={'current': 20, 'total': 100})
    time.sleep(1)

    basewidth = int(width)
    img = Image.open(StringIO(stringed_image))
    wpercent = (basewidth/float(img.size[0]))
    hsize = int((float(img.size[1])*float(wpercent)))
    current_task.update_state(state='PROGRESS', meta={'current': 40, 'total': 100})
    time.sleep(1)

    img = img.resize((basewidth, hsize), PIL.Image.ANTIALIAS)
    current_task.update_state(state='PROGRESS', meta={'current': 60, 'total': 100})

    output = StringIO()
    img.save(output, format='JPEG', quality=50)
    current_task.update_state(state='PROGRESS', meta={'current': 80, 'total': 100})
    time.sleep(1)

    contents = output.getvalue()
    output.close()
    current_task.update_state(state='PROGRESS', meta={'current': 100, 'total': 100})

    return len(contents), contents